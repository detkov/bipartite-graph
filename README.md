Bipartite Graph.  
This program checks if the graph is bipartite and if yes, shows it's parts.  
In file `in.txt` in first row write amount of nodes. For each node in graph write with which node it is connected in new line like in an examle.  
After running a program you'll see `N` if graph is not bipartite, otherwise `Y` and list of nodes in one part.