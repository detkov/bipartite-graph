import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {
        Graph data = readInput();
        writeOutput(data.isDPG, data.DPG);
    }

    public static Graph readInput() throws IOException {
        List<String> list = new ArrayList<>();
        List<Integer[]> data = new ArrayList<>();
        Integer capacity;

        BufferedReader in = new BufferedReader(new FileReader("in.txt"));
        String s;
        while((s=in.readLine()) != null)
            list.add(s);
        in.close();

        capacity = Integer.parseInt(list.get(0));
        data.add(null);
        for(String each : list.subList(1, list.size())){
            Integer[] nodeAdjacency = new Integer[(each.length()-1)/2];
            if(each.length() > 2) {
                int i = 0;
                for (String integerStr : Arrays.stream(each.split(" ")).limit(nodeAdjacency.length).collect(Collectors.toList())) {
                    nodeAdjacency[i]=(Integer.parseInt(integerStr));
                    i++;
                }
            }
            else
            	continue;
            data.add(nodeAdjacency);
        }
        return new Graph(capacity, data);
    }
    
    public static void writeOutput(boolean IsDoublePartGraph, List<List<Integer>> graph) throws IOException {
        PrintWriter out = new PrintWriter(new FileWriter(new File("out.txt")));

        if(IsDoublePartGraph) {
            out.println("Y");
            for (List<Integer> partion : graph) {
                for(int i=0; i < partion.size(); i++) {
                    if (i == partion.size() - 1)
                        out.write(partion.get(i) + " 0");
                    else
                    	out.write(partion.get(i) + " ");
                }
                out.println();
            }
        }
        else
        	out.write("N");
        out.flush();
        out.close();
    }
}