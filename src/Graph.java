import java.util.*;

public class Graph{
    public int N;
    public List<Integer[]> AdjacentList;
    public List<List<Integer>> DPG;
    public boolean isDPG;

    public Graph(int n, List<Integer[]> list){
        N = n;
        AdjacentList=list;
        DPG = new ArrayList<>(2);
        DPG.add(new ArrayList<>());
        DPG.add(new ArrayList<>());
        
        isDPG = FindDPG();
        if(isDPG) {
            DPG.get(0).sort(Integer::compareTo);
            DPG.get(1).sort(Integer::compareTo);
            DPG.sort(Comparator.comparing(x -> x.get(0)));
        }
    }

    private boolean FindDPG(){
        List<Integer> visited = new ArrayList<Integer>();
        Stack<Integer> stack = new Stack<Integer>();
        Integer[] partition = new Integer[N+1];
        Integer[] fathers = new Integer[N+1];
        for(int i=0; i < N+1; i++)
            partition[i]=0;
        partition[1]=1;
        partition[0]=1;
        fathers[1]=0;
        stack.push(1);
        List<Integer> currentGraphPart;
        while (stack.size() != 0) {
            int vertex = stack.pop();
            partition[vertex] = partition[fathers[vertex]] == 1 ? 2 : 1;
            currentGraphPart = DPG.get(partition[vertex]-1);
            for(int eachNode : AdjacentList.get(vertex)) {
                if (partition[eachNode] != 0) {
                    if (partition[eachNode] == partition[vertex])
                        return false;
                }
                if(visited.contains(eachNode) || stack.contains(eachNode)) continue;
                stack.push(eachNode);
                fathers[eachNode] = vertex;
            }
            currentGraphPart.add(vertex);
            visited.add(vertex);
        }
        return true;
    }
}